.TH masqmail.conf 5 2012-01-18 masqmail-0.3.4 "File Formats"

.SH NAME
masqmail.conf \- masqmail configuration file


.SH DESCRIPTION

This man page describes the syntax of the main configuration file of masqmail.
Its usual location is \fI/etc/masqmail/masqmail.conf\fR

The configuration consists of lines of the form

\fBval\fR = \fIexpression\fR

Where \fBval\fR is a variable name and \fIexpression\fR a string,
which can be quoted with double quotes `"'.
If the expression is on multiple lines or contains characters other than letters,
digits or the characters `.', `-', `_', `/', ';', '@', ':', it must be quoted.
You can use quotes inside quotes by escaping them with a backslash.

Each \fBval\fP has a type, which can be boolean, numeric, string or list.
A boolean variable can be set with one of the values `on', `yes', and `true' or `off', `no' and `false'.
List items are separated with semicolons `;'.
For some values, patterns (like `*',`?') can be used.
The spaces in front of and after the equal sign `=' are optional.

Most lists (exceptions: \fBlocal_hosts\fR, \fBlisten_addresses\fR,
\fBquery_routes.\fIname\fR and \fBpermanent_routes\fR) accept files.
These will be recognized by a leading slash `/'.
The contents of these files will be included at the position of the file name,
there can be items or other files before and after the file entry.
The format of the files is different though, within these files each entry is on another line
and the entries are not separated by semicolons.
This makes it easy to include large lists which are common in different configuration files,
so they do not have to appear in every configuration file.

Blank lines and lines starting with a hash `#' are ignored.


.SH OPTIONS

.TP
\fBrun_as_user = \fIboolean\fR

If this is set, masqmail runs with the user id of the user who invoked it and never changes it.
This is for debugging purposes only.
If the user is not root, masqmail will not be able to listen on a port < 1024
and will not be able to deliver local mail to others than the user.

.TP
\fBuse_syslog = \fIboolean\fR

If this is set, masqmail uses syslogd for logging.
It uses facility MAIL.
You still have to set \fBlog_dir\fR for debug files.

.TP
\fBdebug_level = \fIn\fR

Set the debug level.
Valid values are 0 to 6 and 9.
Be careful if you set this as high as 5 or higher,
the logs may very soon fill your hard drive.
Level 9 enables printing of debug messages to stderr during reading of
the config file.
The debug file comes available for the first time after this step.
Thus nothing but stderr is available.
Level 9 is almost never interesting.

.TP
\fBlog_dir = \fIfile\fR

The directory where logs are stored, if syslog is not used.
Debug files are always stored in this directory if debugging is enabled.
\fIfile\fR must be an absolute path.

Default: \fI/var/log/masqmail\fR

.TP
\fBmail_dir = \fIfile\fR

The directory where local mail is stored, usually \fI/var/spool/mail\fR or \fI/var/mail\fR.
\fIfile\fR must be an absolute path.

Default: \fI/var/mail\fR

.TP
\fBspool_dir = \fIfile\fR

The directory where masqmail stores its spool files (and later also other stuff).
It must have a subdirectory \fIinput\fR.
Masqmail needs read and write permissions for this directory.
\fIfile\fR must be an absolute path.

Default: \fI/var/spool/masqmail\fR

.TP
\fBlock_dir = \fIfile\fR

The directory where masqmail stores its lock files.
Masqmail needs read and write permissions for this directory.
By default it is a directory ``lock'' inside of \fIspool_dir\fP.
\fIfile\fR must be an absolute path.

.TP
\fBhost_name = \fIstring\fR

This is used in different places: Masqmail identifies itself in the greeting banner
on incoming connections and in the HELO/EHLO command for outgoing connections with this name,
it is used in the Received: header and to qualify the sender of a locally originating message.

If the string begins with a slash `/', it it assumed that it is a filename,
and the first line of this file will be used.
Usually this will be `/etc/mailname' to make masqmail conform to Debian policies.

It is not used to find whether an address is local. Use \fBlocal_hosts\fR for that.

Default: none; \fBhost_name\fP MUST be set in the config file

.TP
\fBlocal_hosts = \fIlist\fR

A semicolon `;' separated list of hostnames which are considered local.
Can contain glob patterns, like
`*example.org' or `mail?.*mydomain.net'.
Normally you should set it to "localhost;foo;foo.bar.com" if your host has the
fully qualified domain name `foo.bar.com'.

Default: localhost ; <value of \fBhost_name\fR cut at the first dot> ; <value of \fBhost_name\fR>

Example: \fIlocalhost;foo;foo.example.org\fR
(if you have set \fBhost_name\fR to \fIfoo.example.org\fR)

.TP
\fBlocal_addresses = \fIlist\fR

A semicolon `;' separated list of fully qualified email-addresses which are
considered local although their domain name part is not in the list of \fBlocal_hosts\fR. 
This list can be seen as an addition to \fBlocal_hosts\fP.

Further more only the local part of the addresses will be regarded,
seeing it as a local user.

Example: \fIlocal_addresses = "person1@yourdomain;person2@yourdomain"\fP

This means mail to person1@yourdomain will effectively go to
person1@localhost, if not redirected by an alias.

.TP
\fBnot_local_addresses = \fIlist\fR

A semicolon `;' separated list of fully qualified email-addresses which are
considered not local although their domain name part is in the list of \fBlocal_hosts\fR. 
This list can be seen as a substraction to \fBlocal_hosts\fP.

This is the opposite of the previous case.
The majority of addresses of a specific domain are local.
But some users are not.
With this option you can easily exclude these users.

Example:

local_hosts = "localhost;myhost;mydomain.net"

not_local_addresses = "eric@mydomain.net"

.TP
\fBlisten_addresses = \fIlist\fR

A semicolon `;' separated list of interfaces on which connections will be accepted.
An interface ist defined by a hostname, optionally followed by a colon `:' and a number for the port.
If this is left out, port 25 will be used.

You can set this to "localhost:25;foo:25" if your hostname is `foo'.

Note that the names are resolved to IP addresses.
If your host has different names which resolve to the same IP,
use only one of them, otherwise you will get an error message.

Default: \fIlocalhost:25\fR (i.e. only local processes can connect)

.TP
\fBdo_save_envelope_to = \fIboolean\fR

If this is set to true, a possibly existing Envelope-to: header in an incoming mail
which is received via either pop3 or smtp will be saved as an X-Orig-Envelope-to: header.

This is useful if you retrieve mail from a pop3 server with fetchmail,
and the server supports Envelope-to: headers,
and you want to make use of those with a mail filtering tool, e.g. procmail.
It cannot be preserved because masqmail sets such a header by itself.

Default is false.

.TP
\fBdo_relay = \fIboolean\fR

If this is set to false, mail with a return path that is not local and a destination
that is also not local will not be accepted via smtp and a 550 reply will be given.
Default is true.

Note that this will not protect you from spammers using open relays,
but from users unable to set their address in their mail clients.

.TP
\fBdo_queue = \fIboolean\fR

If this is set, masqmail will not try to deliver mail immediately when accepted.
Instead it will always queue it.
(Note: Masqmail will always automatically queue mail if necessary,
i.e. if it cannot deliver because no suitable route was available for example.)

Same as calling masqmail with the \fB\-odq\fR option.
Usually you should leave this option unset.

Default: false

.TP
\fBpermanent_routes\fR = \fIlist\fR

Set this to the filename (or a semicolon-separated list of filenames)
of the route configuration for always available connections.
Main purpose is to define a mail server with mail_host in your local network,
or if masqmail should send mail directly to the target host.
If you have only a single host, you can leave it unset.

A setting `\fBlocal_nets\fR = \fI"*home.net"\fR' in versions <= 0.3.3
is in newer versions configured as:
`\fBpermanent_routes\fR = \fI"/etc/masqmail/homenet.route"\fR'
and the route file `homenet.route' containing:
.in +1in
.nf
allowed_recipients = "*@*home.net"
connect_error_fail = true
resolve_list = byname
.fi
.in 0
This is just as it had been with \fBlocal_net_route\fP,
with the exception that the filtering for appropriate addresses
is only in the route file and not with \fBlocal_nets\fR.

.TP
\fBquery_routes.\fIname\fR = \fIlist\fR

Replace \fIname\fR with a name to identify the connection.
Set this to a filename (or a semicolon-separated list of filenames)
for the route configuration for that connection.

Routes of this kind cannot be expected to be online always.
Masqmail will query which of the routes are online.

You can use the name to call masqmail with the \fB\-qo\fR option every time a
connection to your ISP is set up, in order to send queued mail through this 
route.

Example: Your ISP has the name FastNet.
Then you write the following line in the main configuration:

\fBquery_routes.\fBFastNet\fR = \fI"/etc/masqmail/fastnet.route"\fR

\fI/etc/masqmail/fastnet.route\fR is the route configuration file,
see \fBmasqmail.route(5)\fR.
As soon as a link to FastNet has been set up,
you call `masqmail \fB\-qo \fIFastNet\fR'.
Masqmail will then read the specified file and send the mails.

See \fBonline_query\fP.

.TP
\fBalias_file = \fIfile\fR

Set this to the location of your alias file.
If not set, no aliasing will be done.

Default: <not set> (i.e. no aliasing is done)

.TP
\fBcaseless_matching = \fIboolean\fR

If this is set, aliasing and the matching for \fBlocal_addresses\fP and
\fBnot_local_addresses\fP will be done caseless.

Note: Be sure to change this option only if the queue is empty as
correct processing of queued messages is not guaranteed otherwise.

Default: false

.TP
\fBpipe_fromline = \fIboolean\fR

If this is set, a from line will be prepended to the output stream whenever
a pipe command is called after an alias expansion.
Default is false.

.TP
\fBpipe_fromhack = \fIboolean\fR

If this is set, each line beginning with `From ' is replaced with `>From '
whenever a pipe command is called after an alias expansion.
You probably want this if you have set \fBpipe_fromline\fR above.
Default is false.

.TP
\fBmbox_default = \fIstring\fR

The default local delivery method.
Can be mbox or mda.
You can override this for each user by using the \fBmbox_users\fR or \fBmda_users\fR (see below).

Default: mbox.

.TP
\fBmbox_users = \fIlist\fR

A list of users which wish delivery to an mbox style mail folder.

.TP
\fBmda_users = \fIlist\fR

A list of users which wish local delivery to an mda.
You have to set \fBmda\fR (see below) as well.

.TP
\fBmda = \fIexpand string\fR

If you want local delivery to be transferred to an mda (Mail Delivery Agent),
set this to a command.
The argument will be expanded on delivery time,
you can use variables beginning with a dolloar sign `$', optionally enclosed in curly braces.
Variables you can use are:

uid - the unique message id.
This is not necessarily identical with the Message ID as given in the Message ID: header.

received_host - the host the mail was received from

ident - the user id of the sender if the message was received locally.

return_path_local - the local part of the return path (sender).

return_path_domain - the domain part of the return path (sender).

return_path - the complete return path (sender).

rcpt_local - the local part of the recipient.

rcpt_domain - the domain part of the recipient.

rcpt - the complete recipient address.

Example:

mda="/usr/bin/procmail \-Y \-d ${rcpt_local}"

For the mda, as for pipe commands, a few environment variables will be set as well.
See \fBmasqmail(8)\fR.
To use environment variables for the mda, the dollar sign `$' has to be escaped with a backslash,
otherwise they will be tried to be expanded with the internal variables.

.TP
\fBmda_fromline = \fIboolean\fR

If this is set, a from line will be prepended to the output stream whenever
a message is delivered to an mda.
Default is false.

.TP
\fBmda_fromhack = \fIboolean\fR

If this is set, each line beginning with `From ' is replaced with `>From '
whenever a message is delivered to an mda.
You probably want this if you have set \fBmda_fromline\fR above.
Default is false.

.TP
\fBonline_query = \fIcommand line\fR

Defines the method masqmail uses to detect whether there exists an online connection currently.

Masqmail executes the command given and reads from its standard output.
The command should just print a route name, as defined
with \fBquery_routes.\fIname\fR, to standard output and return a zero status code.
Masqmail assumes it is offline if the script returns with a non-zero status.
Leading and trailing whitespace is removed from the output.

Simple example:

.nf
#!/bin/sh
test \-e /var/run/masqmail/masqmail-route || exit 1
cat /var/run/masqmail/masqmail-route
exit 0
.fi

No matter how masqmail detects the online status,
only messages that are accepted at online time will be delivered using the connection.
The mail spool still needs to be emptied manually
(\fB\-qo\fIconnection\fR).

\fIcommand line\fR must start with an absolute path to an executable program.
It can contain optional arguments.

To simulate the old online_method=file, use:
\fI/bin/cat /path/to/file\fP

To be always online with connection `foo', use:
\fI/bin/echo foo\fP

To query a masqdialer server
(i.e. asking it whether a connection exists and what its name is)
use:
\fI/usr/bin/mservdetect localhost 224\fP

.TP
\fBerrmsg_file = \fIfile\fR

Set this to a template which will be used to generate delivery failure reports.
Variable parts within the template begin with a dollar sign and are identical
to those which can be used as arguments for the mda command, see \fBmda\fR above.
Additional information can be included with @failed_rcpts, @msg_headers and @msg_body,
these must be at the beginning of a line and will be replaced with the list of the failed recipients,
the message headers and the message body of the failed message.

Default is /usr/share/masqmail/tpl/failmsg.tpl.

.TP
\fBwarnmsg_file = \fIfile\fR

Set this to a template which will be used to generate delivery warning reports.
It uses the same mechanisms for variables as \fBerrmsg_file\fR, see above.

Default is /usr/share/masqmail/tpl/warnmsg.tpl.

.TP
\fBwarn_intervals\fR = \fIlist\fR

Set this to a list of time intervals, at which delivery warnings
(starting with the receiving time of the message) shall be generated.

A warning will only be generated just after an attempt to deliver the mail
and if that attempt failed temporarily.
So a warning may be generated after a longer time, if there was no attempt before.

Default is "1h;4h;8h;1d;2d;3d"

.TP
\fBmax_defer_time\fR = \fItime\fR

This is the maximum time, in which a temporarily failed mail will be kept in the spool.
When this time is exceeded, it will be handled as a delivery failure,
and the message will be bounced.

The excedence of this time will only be noticed if the message was actually tried to be delivered.
If, for example, the message can only be delivered when online,
but you have not been online for that time, no bounce will be generated.

Default is 4d (4 days)

.TP
\fBlog_user = \fIname\fR

Replace \fIname\fR with a valid local or remote mail address.

If this option is set, then a copy of every mail,
that passes through the masqmail system will also be sent to the given mail address.

For example you can feed your mails into a program like hypermail
for archiving purpose by placing an appropriate pipe command in masqmail.alias

.TP
\fBmax_msg_size\fR = \fIbytes\fR

This option sets the maximum size in bytes masqmail will accept for delivery.
This value is advertised to the SMTP client by the `SIZE' message during SMTP
session setup.
Clients pretending to send, or actually send,
more than \fIbytes\fR will get a 552 error message.

`0' means no fixed maximum size limit is in force.

Default is 0 (= unlimited).

.TP
\fBdefer_all\fR = \fIboolean\fR

If set to true, masqmail replies with ``421 service temporarily unavailable''
to any SMTP request and shuts the connection down.
Note: This option is for debugging purposes only.

Default: false


.SH AUTHOR

Masqmail was written by Oliver Kurth.
It is now maintained by Markus Schnalke <meillo@marmaro.de>.

You will find the newest version of masqmail at \fBhttp://marmaro.de/prog/masqmail/\fR.
There is also a mailing list, you will find information about it at masqmail's main site.


.SH BUGS

Please report bugs to the mailing list.


.SH SEE ALSO

\fBmasqmail(8)\fR, \fBmasqmail.route(5)\fR
