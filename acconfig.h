#undef DEF_MAIL_GROUP
#undef DEF_MAIL_USER

/* Define if you want to use lockfile library.  */
#undef USE_LIBLOCKFILE

#undef ENABLE_DEBUG

#undef ENABLE_RESOLVER

#undef ENABLE_AUTH

#undef CONF_DIR

#undef DATA_DIR

#undef SBINDIR
